## Cara menjalankan aplikasi

Catatan: **pastikan php, mysql, nodejs (npm) dan composer sudah berjalan dikomputer anda**

1.  buka terminal windows/linux/macos anda
2.  ketikan perintah `git clone https://gitlab.com/cuinc99/test-senior-backend-dev-go-kampus.git`
3.  masukan ke folder aplikasi `cd test-senior-backend-dev-go-kampus`
4.  ketikan perintah `composer install`
5.  duplikasi file **.env.example** dan rubah namanya menjadi **.env**
6.  ketikan perintah `php artisan key:generate`
7.  ketika perintah `php artisan storage:link`
8.  edit file **.env** dan sesuaikan data dibawah ini

    ```sh
    DB_PORT=3306 #ganti dengan port mysql anda
    DB_DATABASE=test_gokampus #nama database kosongan
    DB_USERNAME=root #ganti dengan nama user mysql anda
    DB_PASSWORD= #ganti dengan password user mysql anda (apabila ada)
    ```

9.  ketika perintah `php artisan migrate --seed`
10. ketika perintah `npm install`
11. ketika perintah `npm run dev`
12. ketika perintah `php artisan serve`
13. buka browser anda dan ketika alamat `http://localhost:8000`
14. masukkkan email dan password untuk login
    email: **admin@admin.com**
    password: **password**
