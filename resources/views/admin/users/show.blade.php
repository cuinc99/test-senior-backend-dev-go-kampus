<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between">
            <div>
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    {{ __('User Detail') }}
                </h2>
            </div>
            <x-secondary-button-link href="{{ route('users.index') }}">
                {{ __('Back') }}
            </x-secondary-button-link>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                <div class="overflow-hidden overflow-x-auto p-6 bg-white border-b border-gray-200">
                    <div class="min-w-full align-middle">
                        <div>
                            Name: {{ $user?->name }}
                        </div>
                        <div>
                            Email: {{ $user?->email }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
