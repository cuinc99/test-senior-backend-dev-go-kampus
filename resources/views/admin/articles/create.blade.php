<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create Article') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                <div class="p-6 bg-white border-b border-gray-200">

                    <x-validation-errors class="mb-4" :errors="$errors" />

                    <form action="{{ route('articles.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="mb-4">
                            <x-input-label for="title" :value="__('Title')" />

                            <x-text-input id="title" class="block mt-1 w-full" type="text" name="title"
                                :value="old('title', '')" required />
                        </div>

                        <div class="mb-4">
                            <x-input-label for="content" :value="__('Content')" />

                            <x-textarea-input id="content" class="block mt-1 w-full" name="content">
                            </x-textarea-input>
                        </div>

                        <div class="mb-4">
                            <x-input-label for="image_path" :value="__('Image')" />

                            <x-file-input id="image_path" class="block mt-1 w-full" type="file" name="image_path"
                                :value="old('image_path', '')" accept="image/*" required />
                        </div>

                        <x-primary-button>
                            {{ __('Submit') }}
                        </x-primary-button>
                        <x-secondary-button-link href="{{ route('articles.index') }}">
                            {{ __('Cancel') }}
                        </x-secondary-button-link>
                    </form>

                </div>

            </div>
        </div>
    </div>
</x-app-layout>
