<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between">
            <div>
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    {{ __($article->title) }}
                </h2>
                <h5 class="font-light text-base text-gray-500 leading-tight">
                    By {{ __($article->article_creator?->name) }}
                </h5>
            </div>
            <x-secondary-button-link href="{{ route('articles.index') }}">
                {{ __('Back') }}
            </x-secondary-button-link>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                <div class="overflow-hidden overflow-x-auto p-6 bg-white border-b border-gray-200">
                    <div class="min-w-full align-middle">
                        <div>
                            <img src="{{ $article->article_image }}" alt="">
                        </div>
                        <div>
                            {{ $article?->content }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
