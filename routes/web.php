<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ArticleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', 'login', 301);

Route::middleware(['auth'])
    ->group(function () {
        Route::get('articles/{article}/delete', [ArticleController::class, 'destroy'])->name('articles.destroy');
        Route::resource('articles', ArticleController::class)->except(['destroy']);

        Route::get('users/{user}/delete', [UserController::class, 'destroy'])->name('users.destroy');
        Route::resource('users', UserController::class)->except(['destroy']);
    });

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

require __DIR__.'/auth.php';
