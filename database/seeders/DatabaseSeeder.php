<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\Models\User::create([
            'name' => 'Administrator',
            'email' => 'admin@admin.com',
            'password' => bcrypt('password'),
        ]);

        for ($i=0; $i < 20; $i++) {
            \App\Models\Article::create([
                'title' => fake()->sentence(),
                'content' => fake()->paragraph(),
                'user_id' => $user->id,
            ]);
        }
    }
}
