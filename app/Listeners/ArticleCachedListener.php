<?php

namespace App\Listeners;

use App\Events\ArticleCached;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ArticleCachedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ArticleCached $event)
    {
        $ttl = 60 * 60 * 24;
        $key = 'cacheArticle' . $event->article?->id;

        Cache::forget($key);

        Cache::add($key, $event->article, $ttl);
    }
}
