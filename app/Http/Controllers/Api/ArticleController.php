<?php

namespace App\Http\Controllers\Api;

use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ArticleResource;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $paginate = $request->_paginate ?? 10;

        $articles = Article::query()
            ->with('article_creator')
            ->latest()
            ->paginate($paginate);

        return ArticleResource::collection($articles);
    }
}
