<?php

namespace App\Http\Controllers;

use App\Events\ArticleCached;
use App\Models\Article;
use Illuminate\Support\Facades\Cache;
use App\Http\Requests\StoreArticleRequest;
use App\Http\Requests\UpdateArticleRequest;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::query()
            ->with('article_creator')
            ->latest()
            ->paginate(10);

        return view('admin.articles.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreArticleRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreArticleRequest $request)
    {
        $validReq = $request->validated();

        if ($request->hasFile('image_path')) {
            $path = $validReq['image_path']->store('article_image', 'public');
            $validReq['image_path'] = $path;
        }

        $article = Article::create($validReq);

        event(new ArticleCached($article));

        return redirect()
            ->route('articles.index')
            ->with('success', 'Article cretaed.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show($articleId)
    {
        $key = 'cacheArticle' . $articleId;
        $article = Cache::get($key);
        if (!$article) {
            $article = Article::findOrFail($articleId);
            event(new ArticleCached($article));
        }
        return view('admin.articles.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        return view('admin.articles.edit', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateArticleRequest  $request
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateArticleRequest $request, Article $article)
    {
        $validReq = $request->validated();

        if ($request->hasFile('image_path')) {
            $path = $validReq['image_path']->store('article_image', 'public');
            $validReq['image_path'] = $path;
        }

        $article->update($validReq);

        event(new ArticleCached($article));

        return redirect()
            ->route('articles.index')
            ->with('success', 'Article updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->delete();

        return back()
            ->with('success', 'Article deleted.');;
    }
}
