<?php

namespace App\Models;

use App\Events\ArticleCached;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Article extends Model
{
    use HasFactory;

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->user_id = auth()->id() ?? 1;
        });
    }

    protected $appends = ['article_image'];

    protected $fillable = [
        'title',
        'content',
        'image_path',
        'user_id',
        'created_at',
        'updated_at'
    ];

    public function getArticleImageAttribute()
    {
        if ($this->image_path) {
            return url('storage/' . $this->image_path);
        }
        return url('img/default.png');
    }

    /**
     * Get the user that owns the Article
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function article_creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
